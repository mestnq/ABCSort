﻿using System;
using System.Collections.Generic;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] data =
            {
                "Austin", "Basil", "Bennet", "Christopher", "Dennis", "Martin", "Fabian", "Hilary", "Quentin",
                "Valentine"
            };
            ABSSort(data, 0);
        }

        public static ICollection<string> ABSSort(ICollection<string> words, int rank)
        {
            if (words.Count <= 1)
                return words;

            var square = new Dictionary<char, List<string>>(62); //заглавные и большие, но слово не может начинаться с Ь Ъ
            var result = new List<string>();
            int shortWordsCounter = 0;
            foreach (var word in words)
            {
                if (rank < word.Length)
                {
                    if (square.ContainsKey(word[rank]))
                        square[word[rank]].Add(word);
                    else
                        square.Add(word[rank], new List<string> {word});
                }
                else
                {
                    result.Add(word);
                    shortWordsCounter++;
                }
            }

            if (shortWordsCounter == words.Count)
                return words;

            for (char i = 'A'; i <= 'z'; i++)
            {
                if (square.ContainsKey(i))
                {
                    foreach (var word in ABSSort(square[i], rank + 1))
                        result.Add(word);
                }
            }

            return result;
        }
    }
}